# Pokemons scraper in Go

Sraping example in [Golang](https://go.dev/) based on the article from [ZenRows](https://www.zenrows.com/blog/web-scraping-golang) and using the [Colly](http://go-colly.org/) scraping framework.

To execute the scraper:
```bash
$ cd web-scraper-go
$ go run scraper.go

Visiting https://scrapeme.live/shop/
Pokemons: 755 - resultsPerPage: 16
Number of pages:  48
48 pages to scrap
755 pokemon founds
Writing output to ./product.csv
```
