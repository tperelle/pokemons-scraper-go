package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"

	"github.com/gocolly/colly"
)

// Declaration of pokemon type
type PokemonProduct struct {
	url, image, name, price string
}

// Globale variables
var websiteUrl = "https://scrapeme.live/shop/"

// Find the number of pages
func getPagesNumber() int {
	var pageNumber int = 0
	var sResultCounts string
	var sResultNumber string
	var sResultsPerPage string
	var n float64

	// Initialize the collector
	c := colly.NewCollector()

	c.OnHTML("p.woocommerce-result-count", func(e *colly.HTMLElement) {
		sResultCounts = e.Text
	})

	c.Visit(websiteUrl)

	// Parse the string to calculate page number
	s := strings.Split(sResultCounts, " ")
	sResultNumber = s[3]
	sResultsPerPage = strings.Split(s[1], "–")[1]

	fmt.Println("Pokemons: " + sResultNumber + " - resultsPerPage: " + sResultsPerPage)

	intResultNumber, err := strconv.Atoi(sResultNumber)
	if err != nil {
		panic(err)
	}
	intresultsPerPage, err := strconv.Atoi(sResultsPerPage)
	if err != nil {
		panic(err)
	}

	n = math.Ceil(float64(intResultNumber) / float64(intresultsPerPage))
	pageNumber = int(n)
	fmt.Println("Number of pages: ", pageNumber)
	return pageNumber
}

// Write a pokemons list in a CSV file
func exportCSV(output string, list []PokemonProduct) {
	// opening the CSV file
	file, err := os.Create(output)
	if err != nil {
		log.Fatalln("Failed to create output CSV file", err)
	}
	defer file.Close()

	// initializing a file writer
	writer := csv.NewWriter(file)

	// defining the CSV headers
	headers := []string{
		"url",
		"image",
		"name",
		"price",
	}
	// writing the column headers
	writer.Write(headers)

	// adding each Pokemon product to the CSV output file
	for _, pokemonProduct := range list {
		// converting a PokemonProduct to an array of strings
		record := []string{
			pokemonProduct.url,
			pokemonProduct.image,
			pokemonProduct.name,
			pokemonProduct.price,
		}

		// writing a new CSV record
		writer.Write(record)
	}
	defer writer.Flush()
}

// Scrap the website
func main() {
	var pokemonProducts []PokemonProduct
	pokemonProduct := PokemonProduct{}

	fmt.Println("Visiting " + websiteUrl)

	// Get the number of pages to scrap
	pagesNumber := getPagesNumber()
	fmt.Println(strconv.Itoa(pagesNumber) + " pages to scrap")

	for i := 1; i <= pagesNumber; i++ {
		// Initialize the collector
		c := colly.NewCollector()

		// Scraping
		c.OnHTML("li.product", func(e *colly.HTMLElement) {
			//fmt.Println("New product found: " + e.ChildText("h2"))

			pokemonProduct.url = e.ChildAttr("a", "href")
			pokemonProduct.image = e.ChildAttr("img", "src")
			pokemonProduct.name = e.ChildText("h2")
			pokemonProduct.price = e.ChildText(".price")

			pokemonProducts = append(pokemonProducts, pokemonProduct)
		})

		c.OnError(func(_ *colly.Response, err error) {
			fmt.Println("Something went wrong: ", err)
		})

		// Connection
		c.Visit(websiteUrl + "page/" + strconv.Itoa(i))
	}

	// Export
	if len(pokemonProducts) > 0 {
		fmt.Println(strconv.Itoa((len(pokemonProducts))) + " pokemon founds")

		outputFile := "./product.csv"
		fmt.Println("Writing output to " + outputFile)
		exportCSV(outputFile, pokemonProducts)
	} else {
		fmt.Println("No pokemons found, no export")
	}
}
